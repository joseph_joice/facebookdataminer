package com.joseph.FbMiner;


import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.conf.Configurable;
import org.apache.flume.event.EventBuilder;
import org.apache.flume.source.AbstractSource;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

/**
 * Created by joseph on 6/5/15.
 */
public class MainClass extends AbstractSource
        implements EventDrivenSource, Configurable  {
   static  Set<String> handles;
   private static Jedis jedis;
    public String getDailyUrl() {
        return dailyUrl;
    }

    public void setDailyUrl(String dailyUrl) {
        this.dailyUrl = dailyUrl;
    }
private ChannelProcessor channel;
    private String dailyUrl;
    public static int countOfPosts = 0;
    JSONArray array;
    Logger logger=Logger.getLogger(MainClass.class);

    private static String accessToken = "";

    public static String getAccessToken() {
        return accessToken;
    }

    public static void setAccessToken(String accessToken) {
        MainClass.accessToken = accessToken;
    }


    public void getNewHandles() {
        logger.info("In handle addition");
        handles=jedis.smembers("facebookDomains");
        StringBuffer output = new StringBuffer();
       String Id="";

            for (String handle:handles) {


                if (!handle.isEmpty()) {
                    Id = getIdFromHandle(handle);
                    logger.info("the handle is" + handle+" "+Id);
                }
                if (!Id.isEmpty()) {
                    logger.info("added "+handle);
                    jedis.sadd("fbids", Id);
                    jedis.srem("facebookDomains", handle);

                }
            }





    }


    ThreadPoolExecutor executor;
    public void firstCall() {

        int i=0;
        Set<String> Ids=jedis.smembers("fbids");
        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(2);
        executor = new ThreadPoolExecutor(1,5,Long.MAX_VALUE,TimeUnit.MILLISECONDS,blockingQueue);
        executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r,
                                          ThreadPoolExecutor executor) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    logger.info("Threadpoool error");
                }
                executor.execute(r);
            }
        });
        for (String handle : Ids) {
            FeedsFromFbPage feedsFromFbPage = new FeedsFromFbPage(this);
            feedsFromFbPage.setHandle(handle);
            feedsFromFbPage.setLink("https://graph.facebook.com/v2.3/" + handle + "/posts?access_token=" + accessToken + "&limits=999");
            executor.execute(feedsFromFbPage);
          //  Thread runthread=new Thread(feedsFromFbPage);
          //  Event event = EventBuilder.withBody((i+++"started Thread").getBytes());
           // getChannelProcessor().processEvent(event);
           // runthread.start();
            //executor.execute(feedsFromFbPage);
        }
        //executor.shutdown();

    }
    public ChannelProcessor getchannel()
    {
        return channel;
    }

    public String getIdFromHandle(String id)
    {


        try {
            String output1;
            StringBuffer output=new StringBuffer();
            URL url=new URL("https://graph.facebook.com/v2.3/" + id+"?access_token="+accessToken);
          //  logger.info(url.toString());
            HttpURLConnection connection=(HttpURLConnection)url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (connection.getInputStream())));

            if (connection.getResponseCode() != 200) {
                logger.info("error "+connection.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : "
                        + connection.getResponseCode());

            }

            while ((output1 = br.readLine()) != null) {

                output.append(output1);
            }
            id=new JSONObject(output.toString()).getString("id");
        } catch (Exception e) {
            logger.info("errortrace" + e.toString());
            e.printStackTrace();
        }
        return id;
    }


//    public static void main(String args[]) {
//
//
//        jedis = new Jedis("107.178.219.43",5866);
//        jedis.auth("ZTNiMGM0NDI5OGZjMWMxNDlhZmJmNGM4OTk2ZmI5");
//        MainClass mainClass=new MainClass();
//        mainClass.getNewHandles();
//        mainClass.firstCall();
//
//
//
//    }

    @Override
    public void configure(Context context) {
        jedis = new Jedis("107.178.219.43",5866);
        jedis.auth(context.getString("redis_auth_key"));
        accessToken = context.getString("facebook_access_token");


    }
    @Override
    public void start() {
        channel=getChannelProcessor();
        getNewHandles();
        firstCall();
        super.start();

    }
    @Override
    public void stop()
    {
        super.stop();
    }
}
