package com.joseph.FbMiner;

import org.apache.flume.Event;
import org.apache.flume.event.EventBuilder;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by joseph on 7/5/15.
 */
public class ParseJson implements Runnable {
    private String json;
    private String paging;
    Thread feedThread;



    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    private String link;
    private MainClass callingMainClass;
    private static final Logger log = Logger.getLogger(ParseJson.class.getName());

    public String getPaging() {
        return paging;
    }

    public void setPaging(String paging) {
        this.paging = paging;
    }


    public ParseJson(String json,MainClass callingMainClass) {
        logger.info("parsing class created "+link);
        this.json = json;
        this.callingMainClass=callingMainClass;


    }

    public void run() {
        parsePosts();

    }
    Logger logger=Logger.getLogger(ParseJson.class);
    public  void  parsePosts() {
        logger.info("in class parsepots "+link);
        boolean flag ;
        String text1="",text2="",text3="",link="";
        JSONObject jsonObject = new JSONObject(json);
        if (jsonObject.has("error"))
            return;
        JSONArray allPosts = jsonObject.getJSONArray("data");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            String dateInString = allPosts.getJSONObject(allPosts.length() - 1).getString("created_time");
            Date date = null;
            try {
                date = sdf.parse(dateInString);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (System.currentTimeMillis() - date.getTime() < 86400000) {
                try {
                    String pagingString = jsonObject.getJSONObject("paging").getString("next");
                    FeedsFromFbPage feedsFromFbPage = new FeedsFromFbPage(callingMainClass);
                    feedsFromFbPage.setHandle(this.getLink());
                    feedsFromFbPage.setLink(pagingString + "&access_token=" + MainClass.getAccessToken());
                   callingMainClass.executor.execute(feedsFromFbPage);
                } catch (NullPointerException e) {
                   logger.info("NullpointerException generated");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < allPosts.length(); i++) {
            text1="";text2="";text3="";link="";
            JSONObject post = allPosts.getJSONObject(i);
            Date dateOfPost = null;
            try {
                dateOfPost = sdf.parse(post.getString("created_time"));
            } catch (ParseException e) {
               // logger.info("error parse");
                e.printStackTrace();
            }
            if (System.currentTimeMillis() - dateOfPost.getTime() < 86400000) {
                MainClass.countOfPosts++;
                 flag = false;
                try {
                    text1=post.getString("message");

                   flag=true;
                } catch (Exception e) {

                }
                try {
                    text2=post.getString("name");
                    if(text1.equals(text2))
                        text2="";
                    flag=true;
                }catch (Exception e)
                {

                }
                try {
                    text3= post.getString("description");
                    if(text3==text2||text3==text1)
                        text3="";
                    flag=true;
                } catch (Exception e) {
                }
                try
                {
                    link=post.getString("link");
                }catch (Exception e){}
                if(flag) {

                    logger.info("Parsed "+MainClass.countOfPosts + " " + " " + link + text1 + "." + text2 + "." + text3);
                    Event event=EventBuilder.withBody((link+text1+text2).getBytes());
                    callingMainClass.getchannel().processEvent(event);
                }
                if(!flag)
                {
                    MainClass.countOfPosts--;
                    logger.info(this.getLink() + "None found " + post.getString("id"));

                }
            }
        }
       // logger.info(System.currentTimeMillis());
    }
}
