package com.joseph.FbMiner;

import org.apache.flume.Event;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.event.EventBuilder;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by joseph on 6/5/15.
 */
public class FeedsFromFbPage implements Runnable{
    private String link;

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    private  String handle;

    private final MainClass callingMainClass;
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    public void run()
    {
        takeFeeds();

    }
    Logger logger=Logger.getLogger(FeedsFromFbPage.class);
   public FeedsFromFbPage(MainClass callingMainClass)
   {
     this.callingMainClass=callingMainClass;
   }
    public void takeFeeds() {
        logger.info("fetching "+link);
        URL url;
        String output="",output1;
        HttpURLConnection conn;
        try {
            url = new URL(link);
           // System.out.println(url.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());

            }

            while ((output1 = br.readLine()) != null) {

                output+=output1;
            }
           logger.info("gonna parse "+link);
            ParseJson parseJson=new ParseJson(output,callingMainClass);
            parseJson.setLink(this.getHandle());
            callingMainClass.executor.execute(parseJson);

        } catch (MalformedURLException e) {

        } catch (ProtocolException e) {

        } catch (IOException e) {

        }
    }
}
